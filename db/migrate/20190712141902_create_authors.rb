class CreateAuthors < ActiveRecord::Migration[5.1]
  def change
    create_table :authors do |t|
      t.string :name
      t.string :main_genre
      t.date :birthdate
      t.text :info

      t.timestamps
    end
  end
end
