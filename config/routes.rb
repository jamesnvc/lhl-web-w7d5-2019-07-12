Rails.application.routes.draw do
  resources :authors, only: [:index, :show] do
    resources :books
  end

  namespace :admin do
    resources :authors
  end
end
