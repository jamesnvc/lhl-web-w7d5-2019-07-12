json.extract! author, :id, :name, :main_genre, :birthdate, :info, :created_at, :updated_at
json.url author_url(author, format: :json)
